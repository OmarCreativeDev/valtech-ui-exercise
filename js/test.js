var section = $('section'),
    sidebarToggler = $('nav li:last-child'),
    sidebarCollapsed = false,
    sidebar = $('aside'),
    grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 260,
        gutter: 20
    });

sidebarToggler.click(function () {
    if(!sidebarCollapsed){
        sidebar.animate({
            width: 'toggle'
        }, function () {
            section.toggleClass('full-width');
            grid.masonry();
        });
    } else {
        section.toggleClass('full-width');
        grid.masonry();
        sidebar.animate({ width: 'toggle' });
    }

    sidebarCollapsed = !sidebarCollapsed;
})